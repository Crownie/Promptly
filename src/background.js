chrome.app.runtime.onLaunched.addListener(function () {


    chrome.system.display.getInfo(function (displays) {
        var d = displays[0];
        var isMainDisplay = true
        if (displays[1] != undefined) {
            d = displays[1];
            isMainDisplay = false;
        }

        chrome.app.window.create('display.html', {
            id:'projection-window',
            'outerBounds': {
                'width': d.bounds.width,
                'height': d.bounds.height,
                'top': d.bounds.top,
                'left': d.bounds.left
            },
            'state': isMainDisplay?'normal':'fullscreen'
        });

        chrome.app.window.create('window.html', {
            id:'main-window',
            'outerBounds': {
                'width': 790,
                'height': 650
            }
        },function(mainWindow){
            mainWindow.onClosed.addListener(function(){
                var projectionWindow = chrome.app.window.get('projection-window');
                if(projectionWindow!=undefined){
                    projectionWindow.close();
                }
            });
        });

    });


});