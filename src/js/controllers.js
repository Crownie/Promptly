/**
 * Created by Steve on 01/07/2016.
 */

PromptlyApp.controller('MainController', function ($scope, agendaService) {

    $scope.timerRunning = false;

    $scope.autoAdvance = false;

    $scope.form = {
        title: '',
        presenter: '',
        duration: 0
    };

    //$scope.displayScreen = undefined;//interfaces with display-screen directive


    agendaService.getAgendaItems(function (agendaItems) {
        $scope.agendaItems = agendaItems;
    });
    $scope.selectedAgendaItem = undefined;

    $scope.openAgendaEditor = function () {
        $('.agenda-form-panel').addClass('active');
    };

    $scope.closeAgendaEditor = function () {
        $('.agenda-form-panel').removeClass('active');
        $scope.form = {
            id: undefined,
            title: '',
            presenter: ''
        };
    };

    var validateForm = function () {
        var passed = true;
        $scope.formError = {
            title: '',
            presenter: '',
            duration: ''//minutes
        };

        if ($.trim($scope.form.title).length < 1) {
            $scope.formError.title = 'title is required';
            passed = false;
        } else {

        }

        if ($scope.form.duration == 0) {
            $scope.formError.duration = 'invalid duration';
            passed = false;
        }

        return passed;
    };

    var onSelectAgendaItem = function (agendaItem) {
        if (agendaItem == undefined) {
            return;
        }
        $scope.selectedAgendaItem = agendaItem;
        Countdown.setTime(agendaItem.duration * 60);
        $scope.timerRunning = false;
    };

    $scope.saveAgendaItem = function () {

        if (!validateForm()) {
            return;
        }

        agendaService.addAgendaItem({
            id: $scope.form.id,
            title: $scope.form.title,
            presenter: $scope.form.presenter,
            duration: $scope.form.duration
        });

        if ($scope.selectedAgendaItem != undefined && $scope.form.id == $scope.selectedAgendaItem.id) {
            $scope.selectedAgendaItem.title = $scope.form.title;
            $scope.selectedAgendaItem.presenter = $scope.form.presenter;
            $scope.selectedAgendaItem.duration = $scope.form.duration;
            Countdown.updateTime($scope.selectedAgendaItem.duration * 60);
            $scope.updateScreen();
        }

        $scope.closeAgendaEditor();
        $scope.form = {
            title: '',
            presenter: '',
            duration: 0
        };


    };

    $scope.formatDuration = function (minutes) {
        minutes = minutes || 0;
        var h = parseInt(minutes / 60);
        var m = minutes % 60;

        return (h > 0) ? h + 'h' + m + 'm' : m + 'm';
    };

    $scope.agendaItemDblClick = function (agendaItem) {

        onSelectAgendaItem(agendaService.selectAgendaItem(agendaItem.id));

    };

    $scope.startCountdown = function () {
        $scope.timerRunning = true;
        Countdown.start();
    };


    $scope.stopCountdown = function () {
        $scope.timerRunning = false;
        Countdown.stop();
    };

    $scope.resetCountdown = function () {
        $scope.timerRunning = false;
        Countdown.setTime($scope.selectedAgendaItem.duration * 60);
    };

    $scope.nextAgendaItem = function () {
        onSelectAgendaItem(agendaService.selectNextAgendaItem());
    };
    $scope.previousAgendaItem = function () {
        onSelectAgendaItem(agendaService.selectPreviousAgendaItem());
    };

    Countdown.setListener(function () {
        $scope.updateScreen();
    });

    $scope.updateScreen = function () {
        
        if ($scope.displayScreen != undefined) {
            var obj = {
                title: $scope.selectedAgendaItem.title,
                presenter: $scope.selectedAgendaItem.presenter,
                time: Countdown.getTime(),
                timeF: Countdown.getTimeF(),
                timeUp: Countdown.isTimeUp()
            };
            $scope.displayScreen.update(obj);
            chrome.runtime.sendMessage(obj);
        }

    };


    $scope.openItemOptions = function ($event) {
        var $target = $($event.currentTarget);
        $target.addClass('item-options-open');
        $target.on('mouseleave', function () {
            $target.removeClass('item-options-open');
            $target.off('mouseleave');
        });
    };

    $scope.editAgendaItem = function (agendaItem) {
        $scope.openAgendaEditor();
        $scope.form = agendaItem;
    };

    $scope.deleteAgendaItem = function (agendaItem) {
        agendaService.removeAgendaItem(agendaItem.id);
    };

    $scope.setDisplayToSecondaryMonitor = function () {

        chrome.system.display.getInfo(function (displays) {
            var projectionWindow = chrome.app.window.get('projection-window');

            var d = displays[0];
            var isMainDisplay = true
            if (displays[1] != undefined) {
                d = displays[1];
                isMainDisplay = false;
            }
            projectionWindow.outerBounds.setSize(d.bounds.width, d.bounds.height);
            projectionWindow.outerBounds.setPosition(d.bounds.left, d.bounds.top);
            projectionWindow.fullscreen();
        });
    };

    $scope.toggleAutoAdvance = function(){
        if($scope.autoAdvance){
            $scope.autoAdvance = false;
        }else{
            $scope.autoAdvance = true;
        }
    };


});