/**
 * Created by Steve on 05/07/2016.
 */

var Countdown = (function () {

    var _public = {};

    var time = 0;

    var interval;


    var timeUp = false;
    var delay = 1000;
    var onCountdownEvent = null;

    _public.setTime = function (seconds) {
        _public.reset(true);
        time = seconds;
        fireUpdateEvent();
    };

    _public.updateTime = function (seconds) {

        time = seconds;
        timeUp = false;
        /*
        //incrementing the time by the difference
        if (time + seconds < 0) {
            timeUp = true;
            time += seconds;
            time = time * -1;
        } else {
            time += seconds;
        }*/

        fireUpdateEvent();
    };

    _public.start = function () {
        _public.stop();
        interval = setInterval(function () {
            actionPerformed();
        }, delay);
    };


    _public.stop = function () {
        if (interval) {
            clearInterval(interval);
        }
    };

    _public.reset = function (noEvent) {
        _public.stop();
        time = 0;
        timeUp = false;
        if(!noEvent){
            fireUpdateEvent();
        }
    };

    _public.getTime = function () {
        return time;
    };

    _public.getTimeF = function () {

        var t = new Date(time * 1000);
        var seconds = t.getSeconds();
        var minutes = t.getMinutes();
        var hours = t.getHours();

        var timeStr = pad(hours,2) + ':' + pad(minutes,2) + ':' + pad(seconds,2);

        return (timeUp) ? "-" + timeStr : timeStr;
    };

    _public.setListener = function (listener) {
        onCountdownEvent = listener;
    };

    _public.isTimeUp = function(){
        return timeUp;
    };

    var fireUpdateEvent = function () {
        if (typeof(onCountdownEvent) === 'function') {
            onCountdownEvent.call(_public);
        }
    };

    var actionPerformed = function () {
        // tick;

        if (time == 0) {
            timeUp = true;
        }

        if (timeUp) {
            time++;
        } else {
            time--;
        }
        fireUpdateEvent();
    };

    var pad = function (num, size) {
        var s = "000000000" + num;
        return s.substr(s.length - size);
    };
    return _public;
})();