/**
 * Created by Steve on 01/07/2016.
 */

PromptlyApp
    .directive('durationPicker', function () {


        return {
            restrict: 'E', //E = element, A = attribute, C = class, M = comment
            scope: {
                //@ reads the attribute value, = provides two-way binding, & works with functions
                value: '=?value'
            },
            templateUrl: 'templates/duration-picker.htm',
            controller: function ($scope) {
                $scope.value = 0;
                $scope.hours = 0;
                $scope.minutes = 0;

                var update = function () {
                    if(isNaN(parseInt($scope.value))){
                        $scope.value = 0;
                    }
                    $scope.hours = $scope.pad(parseInt($scope.value / 60), 2);
                    $scope.minutes = $scope.pad(parseInt($scope.value) % 60, 2);
                };

                $scope.pad = function (num, size) {
                    var s = "000000000" + num;
                    return s.substr(s.length - size);
                };


                $scope.up = function () {
                    $scope.value = parseInt($scope.value) + 5;
                    update();
                };

                $scope.down = function () {
                    $scope.value = parseInt($scope.value) - 5;
                    if ($scope.value < 0) {
                        $scope.value = 0;
                    }
                    update();
                };

                $scope.$watch('ready', function () {
                    update();
                });

                $scope.$watch('value', function () {

                        update();
                });


            }, //Embed a custom controller in the directive
            link: function ($scope, element, attrs) {
                var $inputs = element.find('input');
                var $buttons = element.find('button');
                var $hours = $inputs.eq(0);
                var $minutes = $inputs.eq(1);
                var $up = $buttons.eq(0);
                var $down = $buttons.eq(1);

                var filterInput = function (val, max, min) {
                    var cleanNumber = parseInt(val);
                    if (cleanNumber == 'NaN') {
                        cleanNumber = 0;
                    }
                    if (cleanNumber > max) {
                        cleanNumber = max;
                    } else if (cleanNumber < min) {
                        cleanNumber = min;
                    }
                    return cleanNumber;
                };


                $hours.on('change', function (event) {
                    console.log("changed");

                    var hours = filterInput($hours.val(), 24, 0);
                    $scope.value = hours * 60 + parseInt($minutes.val());
                });

                $hours.on('keyup', function (event) {
                    var hours = filterInput($hours.val(), 24, 0)
                    $hours.val($scope.pad(hours, 2));
                    $scope.value = hours * 60 + parseInt($minutes.val());
                    $scope.$apply();
                });

                $minutes.on('change', function (event) {
                    console.log("changed");
                    var minutes = filterInput($minutes.val(), 59, 0);

                    $scope.value = parseInt($hours.val()) * 60 + parseInt(minutes);
                });

                $minutes.on('keyup', function (event) {
                    var minutes = filterInput($minutes.val(), 59, 0);
                    $minutes.val($scope.pad(minutes, 2));
                    $scope.value = parseInt($hours.val()) * 60 + parseInt(minutes);
                    $scope.$apply();
                });

            } //DOM manipulation
        }
    })


    .directive('displayScreen', function () {


        return {
            restrict: 'E', //E = element, A = attribute, C = class, M = comment
            scope: {
                //@ reads the attribute value, = provides two-way binding, & works with functions
                control: '=?control'
            },
            controller: function ($scope) {
                $scope.state = 'normal';
                $scope.title = '-';
                $scope.presenter = '-';
                $scope.timeF = '00:00:00';

                $scope.control = {
                    update: function (obj) {
                        $scope.title = obj.title;
                        $scope.presenter = obj.presenter;
                        $scope.timeF = obj.timeF;

                        if (obj.timeUp) {
                            $scope.state = 'timeup';
                            $('.ds-timer').addClass('ds-timer--negative');
                        } else if (obj.time < 60) {

                            $scope.state = 'warning';
                            $('.ds-timer').removeClass('ds-timer--negative');
                        } else {
                            $scope.state = 'normal';
                            $('.ds-timer').removeClass('ds-timer--negative');
                        }

                        setTimeout(function () {
                            $scope.$apply();
                        }, 0);
                    }
                };

            },
            templateUrl: 'templates/display-screen.htm',
            link: function ($scope, element, attrs) {
                $element = $(element[0]);

                $element.find('.ds-title').flowtype({
                    fontRatio: 14
                });
                $element.find('.ds-presenter').flowtype({
                    fontRatio: 16
                });
                $element.find('.ds-time').flowtype({
                    fontRatio: 6
                });
                $element.find('.ds-timer-label').flowtype({
                    fontRatio: 30
                });


                var calculateAspectRatio = function () {
                    //aspect ratio
                    //w*9/16
                    var height = ($element.width() * 9) / 16;
                    $element.height(height);
                };

                calculateAspectRatio();

                $(window).resize(calculateAspectRatio);

            }
        }
    })


    .directive('ngRightClick', function ($parse) {
        return function (scope, element, attrs) {
            var fn = $parse(attrs.ngRightClick);
            element.bind('contextmenu', function (event) {
                scope.$apply(function () {
                    event.preventDefault();
                    fn(scope, {$event: event});
                });
            });
        };
    });