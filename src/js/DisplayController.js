/**
 * Created by Steve on 05/07/2016.
 */
PromptlyApp.controller('DisplayController', function ($scope) {


    chrome.runtime.onMessage.addListener(
        function (request, sender, sendResponse) {

            if($scope.displayScreen!=undefined){
                $scope.displayScreen.update(request);
            }

        });
});