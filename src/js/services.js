/**
 * Created by Steve on 04/07/2016.
 */

PromptlyApp.factory('agendaService', function () {

    var fac = {};
    var dataLoaded =false;
    var agendaItems = [{
        id: 1,
        title: 'Heloo',
        presenter: 'john snow',
        duration: 100
    },
        {
            id: 2,
            title: 'Lorem Ipsum',
            presenter: 'Lanis',
            duration: 1
        }
    ];
    var selectedAgendaItem = {};

    var saveData = function(){
        chrome.storage.sync.set({'agenda':agendaItems}, function(){
        });
    };

    var loadData = function(callback){
        chrome.storage.sync.get('agenda',function(obj){
            agendaItems = obj.agenda || [];
            dataLoaded = true;
            callback();
        });
    };

    var guid =function () {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                .toString(16)
                .substring(1);
        }
        return s4() + s4()  + s4()  + s4()  +
            s4()  + s4() + s4() + s4();
    };

    var stepSelectAgenda = function(step){
        for (var index in agendaItems) {
            index = parseInt(index);
            if(agendaItems[index].selected){
                agendaItems[index].selected = false;
                var nextIndex = index;
                if(step>0){
                    nextIndex = index<agendaItems.length+(-1*step)?index+step:0;//wraps around if it's last item
                }else if(step<0){
                    nextIndex = index==0?+agendaItems.length+(step):index+(step);//wraps around if it's last item
                }
                agendaItems[nextIndex].selected = true;
                selectedAgendaItem = agendaItems[nextIndex];
                return selectedAgendaItem;
            }
        }
    };

    var editAgendaItem = function(item){
        for (var index in agendaItems) {
            if (agendaItems[index].id == item.id) {
                agendaItems[index].title = item.title;
                agendaItems[index].presenter = item.presenter;
                agendaItems[index].duration = item.duration;
                break;
            }
        }
    };


    fac.addAgendaItem = function (agendaItem) {

        if(agendaItem.id==undefined){
            agendaItem.id = guid();
            agendaItems.push(agendaItem);
        }else{
            editAgendaItem(agendaItem);
        }

        saveData();
    };

    fac.getAgendaItems = function (callback) {
        if(!dataLoaded){
            loadData(function(){
                callback(agendaItems);
            });
        }else{
            callback(agendaItems);
        }
    };

    fac.removeAgendaItem = function(itemId){
        for (var index in agendaItems) {
            if (agendaItems[index].id == itemId) {
                agendaItems.splice(index,1);
                break;
            }
        }
        saveData();
    };

    fac.selectAgendaItem = function (itemId) {
        for (var index in agendaItems) {
            agendaItems[index].selected = false;
            if (agendaItems[index].id == itemId) {
                agendaItems[index].selected = true;
                selectedAgendaItem = agendaItems[index];
            }
        }
        return selectedAgendaItem;
    };

    fac.selectNextAgendaItem = function(){
        return stepSelectAgenda(1);
    };

    fac.selectPreviousAgendaItem = function(){
        return stepSelectAgenda(-1);
    };

    fac.getSelectedAgendaItem = function () {
        return selectedAgendaItem;
    };

    return fac;

});